import { createRouter, createWebHistory } from 'vue-router';
import HomeView from '@/views/HomeView.vue';
import DetailsView from '@/views/DetailsView.vue';
import SeasonEpisodesView from '@/views/SeasonEpisodesView.vue';
import DetailsEpisodeView from '@/views/DetailsEpisodeView.vue';
import JouerEpisodeView from '@/views/JouerEpisodeView.vue';
import LoginView from '@/views/LoginView.vue';
import ProfileView from '@/views/ProfileView.vue';
import SignupView from '@/views/SignupView.vue';
import AboutView from '@/views/AboutView.vue';
import HistoryView from '@/views/HistoryView.vue';

const routes = [{
    path: '/',
    name: 'home',
    component: HomeView,
}, {
    path: '/details/:tvshowId',
    name: 'details',
    component: DetailsView,
}, {
    path: '/season/:seasonId',
    name: 'seasonepisodes',
    component: SeasonEpisodesView,
}, {
    path: '/episodedetails/:episodeId',
    name: 'detailsEpisode',
    component: DetailsEpisodeView,
}, {
    path: '/jouerepisode/:episodeId',
    name: 'jouerEpisode',
    component: JouerEpisodeView,
}, {
    path: '/login',
    name: 'login',
    component: LoginView,
}, {
    path: '/profile',
    name: 'profile',
    component: ProfileView,
}, {
    path: '/signup',
    name: 'signup',
    component: SignupView,
}, {
    path: '/about',
    name: 'about',
    component: AboutView,
}, {
    path: '/history',
    name: 'history',
    component: HistoryView,
},
];

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
});

export default router;
