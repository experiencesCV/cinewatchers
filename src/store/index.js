import { createStore } from 'vuex';

export default createStore({
    state: {
        token: '',
        username: '',
        history: [],
    },
    getters: {},
    mutations: {
        settoken(state, s) {
            state.token = s;
        },
    },
    actions: {
        storetoken(saveToken, s) {
            saveToken.commit('settoken', s);
            sessionStorage.setItem('token', s);
        },
    },
    modules: {},
});
